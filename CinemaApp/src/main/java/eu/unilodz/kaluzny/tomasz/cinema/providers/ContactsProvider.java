package eu.unilodz.kaluzny.tomasz.cinema.providers;


import java.util.Objects;

public class ContactsProvider {

    private static ContactsProvider instance = null;

    public static ContactsProvider getInstance() {
        if (Objects.isNull(instance)) {
            instance = new ContactsProvider();
        }
        return instance;
    }

    private ContactsProvider(){}

    public String getOpenningHours(){
        return "Pn-Pt: 9:00 - 22:00\nSOSob.: 9:00 - 23:00\nNiedz.: 10:00 - 21:00";
    }

    public String getAddress(){
        return "ul. Wólczańska 1\n90-720 Łódź";
    }

    public String getPhoneNumber(){
        return "Biuro: 43 673-56-48\nKasa biletowa: 43 673-52-88";
    }
}
