package eu.unilodz.kaluzny.tomasz.cinema.utils;

import android.content.res.Resources;
import android.util.TypedValue;

public class DpToPxConverter {

    public static int convert(int dp, Resources r){
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));

    }
}
