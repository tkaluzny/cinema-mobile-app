package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.activities.tools.GridSpacingItemDecoration;
import eu.unilodz.kaluzny.tomasz.cinema.activities.tools.ReservationsViewAdapter;
import eu.unilodz.kaluzny.tomasz.cinema.models.ReservationsModel;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesReservations;
import eu.unilodz.kaluzny.tomasz.cinema.utils.DpToPxConverter;
import lombok.Getter;

public class CurrentReservationsActivity extends AbstractNavigationActivity {

    private EditText reservationUserData;
    private Button reservationSearchButton;
    private TextView resWaringMessage;
    private RecyclerView recyclerView;
    private ReservationsViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_reservations);
        init();
        reservationSearchButton.setOnClickListener(onSearchClick());
        adapter = new ReservationsViewAdapter();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, DpToPxConverter.convert(5,getResources()), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private View.OnClickListener onSearchClick(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userData = reservationUserData.getText().toString();
                if(StringUtils.isBlank(userData)){
                    resWaringMessage.setVisibility(View.VISIBLE);
                    return;
                }

                resWaringMessage.setVisibility(View.INVISIBLE);
                List<ReservationsModel> reservationsList =  MoviesReservations.getInstance().getCurrentUserReservations(userData);
                if(reservationsList.isEmpty()){
                    resWaringMessage.setVisibility(View.VISIBLE);
                    return;
                }
                adapter.setReservations(reservationsList);

            }
        };
    }

    private void init(){
        this.reservationUserData = (EditText) findViewById(R.id.reservationUserData);
        this.reservationSearchButton = (Button) findViewById(R.id.searchReservationsButton);
        this.resWaringMessage = (TextView) findViewById(R.id.resWaringMessage);
        this.recyclerView = findViewById(R.id.reservationsRecycleView);

    }
}
