package eu.unilodz.kaluzny.tomasz.cinema.models;

import org.apache.commons.lang3.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserData {

    private String name;
    private String phone;
    private String email;

    public boolean checkUserData(String userData){
        return StringUtils.equals(name, userData) ||
                StringUtils.equals(phone, userData) ||
                StringUtils.equals(email, userData);
    }

}
