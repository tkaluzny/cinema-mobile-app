package eu.unilodz.kaluzny.tomasz.cinema.models;

import android.util.Pair;

import java.util.Set;

import lombok.Data;

@Data
public class TicketModel {

    private TicketType type;
    private Set<Pair<String, Integer>> pricesPairs;

    public enum TicketType{
        NORMAL("Normalny"), STUDENT("Studencki"), JUN_SEN("Junior/Senior"), FAMILY("Rodzinny");

        TicketType(String name) {
            this.name = name;
        }

        public String getName() {

            return name;
        }

        private String name;
    }
}
