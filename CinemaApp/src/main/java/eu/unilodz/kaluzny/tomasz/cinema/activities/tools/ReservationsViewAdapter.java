package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.models.ReservationsModel;

public class ReservationsViewAdapter extends RecyclerView.Adapter<ReservationsViewHolder> {

    private List<ReservationsModel> reservations;

    public ReservationsViewAdapter(){

    }

    public ReservationsViewAdapter(List<ReservationsModel> reservations){
        this.reservations = reservations;
    }

    @NonNull
    @Override
    public ReservationsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.reservations_card_view, viewGroup, false);
        return new ReservationsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationsViewHolder reservationsViewHolder, int i) {
        ReservationsModel reservatin = reservations.get(i);
        reservationsViewHolder.bind(reservatin);
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(reservations);
    }

    public void setReservations(List<ReservationsModel> reservations) {
        this.reservations = reservations;
        notifyDataSetChanged();
    }
}
