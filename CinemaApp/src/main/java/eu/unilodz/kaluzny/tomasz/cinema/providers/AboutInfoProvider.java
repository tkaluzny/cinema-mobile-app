package eu.unilodz.kaluzny.tomasz.cinema.providers;

import android.util.Pair;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AboutInfoProvider {

    private static AboutInfoProvider instance = null;

    ArrayList<Pair<String, String>> info;


    private AboutInfoProvider(){
        info = new ArrayList<>();
        String info1Text = "Poza regularnym repertuarem, w którym prezentowane są największe hity światowej kinematografii, " +
                "nasze kino prezentuje Widzom także seanse specjalne takie jak: Kino Kobiet, Nocne Maratony Filmowe, Kino Konesera czy Wolczanka dla Dzieci.";
        String info1Foto = "https://ftmp.helios.pl/Get/file/cinpicmin/722/1466608919";

        String info2Text = "W menu Wólczanka Café znajdziesz:\n" +
                "\n" +
                "szeroką ofertę ciast,\n" +
                "zimne napoje (pepsi, soki, woda),\n" +
                "świeżo wyciskane soki,\n" +
                "smoothies,\n" +
                "słodkie przekąski,\n" +
                "lody i desery lodowe.\n" +
                "Zapraszamy!";

        String info2Foto = "https://ftmp.helios.pl/Get/file/cinpic/714/1466608813";

        String info3Text = "Kino Wólczanka może pochwalić się sześcioma salami kinowymi o pojemności 30 miejsc siedzących. \nDzięki niewielkim salom, dźwięk " +
                "podczas seansu są wyraziste oraz bardzo czyste. W salach zastosowano najnowszy system nagłośnienia Dolby. \nKażda z sal jest klimatyzowana, dzięki czemu" +
                "seans mija Państwu w komfortowych warunkach";
        String info3Foto = "http://u-jazdowski.pl/upload/thumb/_dsc0381_csw_kinolab_3kpx144_auto_1400x800.jpg";

        info.add(new Pair<String, String>(info1Text, info1Foto));
        info.add(new Pair<String, String>(info2Text, info2Foto));
        info.add(new Pair<String, String>(info3Text, info3Foto));
    }

    public static AboutInfoProvider getInstance(){
        if(Objects.isNull(instance)){
            instance = new AboutInfoProvider();
        }
        return instance;
    }

    public ArrayList<Pair<String, String>> getInfo(){
        return info;
    }


    public Pair<String, String> getInfoById(int id) throws IllegalArgumentException, IndexOutOfBoundsException{
        if(id <0){
            throw new IllegalArgumentException("Id is lower than 0!");
        }
        if(id > info.size() ){
            throw new IndexOutOfBoundsException("Id is greather than info size!");
        }
        return info.get(id);
    }

    public int getInfoSize(){
        return CollectionUtils.size(info);
    }

}
