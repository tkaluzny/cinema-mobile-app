package eu.unilodz.kaluzny.tomasz.cinema.providers;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import lombok.Getter;

public class MoviesProvider {

    private static MoviesProvider instance = null;
    @Getter
    private List<Movie> movies;

    public static MoviesProvider getInstance(){
        if(Objects.isNull(instance)){
            instance = new MoviesProvider();
        }
        return instance;
    }

    public Movie getById(long id){
        for(Movie movie : movies){
            if(id == movie.getId()){
                return movie;
            }
        }
        return null;
    }
    private MoviesProvider(){
        movies = new LinkedList<>();
        Movie m1 = new Movie();
        m1.setId(1);
        m1.setTitle("Robin Hood: Początek");
        m1.setType("Przygodowy");
        m1.setDuration(116 );
        m1.setDescr("Zaprawiony w bojach podczas wypraw krzyżowych Robin z Loxley powraca w rodzinne strony, gdzie odkrywa, że jego zamek został splądrowany, a ziemie skonfiskowane przez chciwego Szeryfa z Nottingham. Co gorsza, ukochana Robina, piękna Marion, dając wiarę fałszywej wieści o jego śmierci, poślubiła innego mężczyznę. Żądny zemsty i sprawiedliwości Robin posta\u00ADnawia stanąć w obronie uciśnionych mieszkańców Nottingham. Jego kom\u00ADpanem i nauczycielem staje się dawny wróg, Mały John. Już wkrótce w mie\u00ADście pojawia się tajemniczy wojownik w saraceńskim kapturze, który odbiera bogatym i rozdaje biednym. Szybko okaże się jednak, że okoliczności wyma\u00ADgają działań na większą skalę, bowiem Szeryf wraz z kościelnymi dostojnikami planują zdradę, która może zagrozić całemu królestwu. Robin musi więc zgromadzić siły zdolne do otwartej walki z regularną armią.");
        m1.setActors(Arrays.asList("Taron Egerton", "Eve Hewson", "Jamie"));
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpstr/13512/1540907950");
        movies.add(m1);

        m1 = new Movie();
        m1.setId(2);
        m1.setTitle("Planeta Singli 2");
        m1.setDuration(120 );
        m1.setType("Komedia romantyczna");
        m1.setDescr("„Planeta Singli 2” to jeden z najbardziej oczekiwanych filmów tego roku – kontynuacja hitu, w którym zakochały się miliony widzów. W listopadzie 2018 na kinowe ekrany powrócą Maciej Stuhr i Agnieszka Więdłocha jako para, której losy połączyła randkowa aplikacja. Na ekranie nie zabraknie wybuchowego małżeństwa granego przez Weronikę Książkiewicz i Tomasza Karolaka oraz wspaniałych: Danuty Stenki, Piotra Głowackiego czy Joanny Jarmołowicz. Szykujcie się na eksplozję humoru, wzruszeń, pozytywnej energii i potężną dawkę przedświątecznej atmosfery. Związek Ani (Agnieszka Więdłocha) i Tomka (Maciej Stuhr) przeżywa poważny kryzys. On, showman-celebryta wcale nie ma zamiaru się ustatkować. Ona z kolei chce poważnego związku. Tymczasem na horyzoncie pojawia się Aleksander – zauroczony Anią milioner, właściciel aplikacji Planeta Singli, przekonany, że nikt inny nie pasuje do niego bardziej niż romantyczna nauczycielka muzyki.");
        m1.setActors(Arrays.asList("Maciej Stuhr", "Agnieszka Więdłocha", "Piotr Głowacki", "Weronika Książkiewicz", "Tomasz Karolak", "Iza Mikko"));
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpstr/13088/1542893735");
        movies.add(m1);

        m1 = new Movie();
        m1.setId(3);
        m1.setTitle("Klakson i spółka");
        m1.setType("Animacja");
        m1.setDuration(116 );
        m1.setDescr("Klakson jest taksówką w wielkiej metropolii zamieszkanej przez auta. Lakier ma nieco zdarty, pod maską czasem coś stuka, ale z reflektorów dobrze mu patrzy. Jak mało kto zna tutejsze ulice, wie z kim trzymać sztamę, a komu lepiej nie wjeżdżać w drogę. Gdy pewna urocza czerwona wyścigówka wpadnie w kłopoty, Klakson będzie musiał stanąć maska w maskę z hersztem gangu wielotonowych ciężarówek, które nie boją się nawet radiowozów. Klakson da z siebie pełną moc, skrzyknie wszystkich blaszanych kumpli i paląc gumy ruszy do boju. Bo gdy ma się odwagę i olej w silniku, niestraszny żaden poślizg.");
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpstr/13510/1540476391");
        movies.add(m1);

        m1 = new Movie();
        m1.setId(4);
        m1.setTitle("Miłość jest wszystkim");
        m1.setType("Komedia romantyczna");
        m1.setDuration(130 );
        m1.setDescr("Grudniowy czas przedświątecznej gorączki rozpocznie się wraz z przypłynięciem do miasta niezwykłego Świętego Mikołaja. Uruchomi to lawinę wyjątkowych zdarzeń. Romans z najpopularniejszym polskim piłkarzem spotka kobietę, której nikt nie nazwałby fanką futbolu. Zdrady i tajemnice zamieszają w niejednym małżeństwie, a dopracowany w każdym szczególe ślub będzie mieć zaskakujący finał.");
        m1.setActors(Arrays.asList("Olaf Lubaszenko", "Agnieszka Grochowska", "Jan Englert"));
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpstr/13095/1540290278");
        movies.add(m1);

        m1 = new Movie();
        m1.setId(5);
        m1.setTitle("Był sobie Deadpool");
        m1.setType("Akcja / Fantasy");
        m1.setDuration(119 );
        m1.setDescr("Niegrzeczny superbohater powraca w baśniowej wersji filmu „Deadpool 2” dla każdego, z 20 minutami nowych scen i uderzeniową dawką niespodzianek. Gdy ją zobaczysz, zaczniesz się zastanawiać, po jaką cholerę ktokolwiek męczył się z produkcją wersji oryginalnej.");
        m1.setActors(Arrays.asList("Ryan Raynolds"));
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpstr/13725/1543233777");
        movies.add(m1);

        m1 = new Movie();
        m1.setId(6);
        m1.setTitle("The Quake. Trzęsienie ziemi");
        m1.setType("Thriller");
        m1.setDuration(109 );
        m1.setDescr("W 1904 roku trzęsienie ziemi o sile 5,4 w skali Richtera nawiedziło stolicę Norwegii – Oslo. Ruchy tektoniczne rejestrowane w Skandynawii wskazują na to, że taka katastrofa nie tylko może się powtórzyć, ale jej siła przekroczy skalę, jakiej dotąd nie rejestrowano na Ziemi.\n" +
                "\n" +
                "Tylko jeden człowiek dostrzeże zagrożenie.\n" +
                "Czy uda mu się uratować najbliższych i ostrzec ludzi przed zbliżającą się katastrofą?\n" +
                "Jaką cenę zapłaci za zaniedbanie władz? \n" +
                "\n" +
                "To będzie zagłada, jakiej świat jeszcze nie widział.");
        m1.setActors(Arrays.asList("Ane Dahl Torp" , "Kristoffer Joner" , "Hang Tran"));
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpic/80877/1543483365");
        movies.add(m1);

        m1 = new Movie();
        m1.setId(7);
        m1.setTitle("Grinch");
        m1.setType("Animacja");
        m1.setDuration(90 );
        m1.setDescr("Adonis Creed stara się pogodzić sprawy osobiste z treningami do najważniejszego bokserskiego starcia w karierze. Będzie musiał stanąć na ringu do walki z przeciwnikiem pośrednio związanym z tragedią, która dotknęła niegdyś jego rodzinę. Adonis ma jednak wciąż w swym narożniku Rocky'ego Balboę, który wspiera go we wszystkich trudnych chwilach. Razem stawią czoło demonom przeszłości, podadzą w wątpliwość wyznawane wartości i staną w obronie wspólnego dziedzictwa. Powrót do korzeni pozwoli im zrozumieć, dlaczego stali się bokserskimi legendami.");
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpstr/12835/1542354475");
        movies.add(m1);

        m1 = new Movie();
        m1.setId(8);
        m1.setTitle("Creed 2 ");
        m1.setType("Akcja");
        m1.setDuration(130 );
        m1.setActors(Arrays.asList("Michael B. Jordan", "Sylvester Stallone", "Tessa Thompson"));
        m1.setDescr("Grinch mieszka w samotności, za jedynego kompana mając psa – Maxa. Opuszcza swą jaskinię, udając się do Ktosiowa, tylko w poszukiwaniu pożywienia.\n" +
                "Każdego roku spokój Ktosiowa zaburza tylko jedno wydarzenie – celebracja świąt Bożego Narodzenia.");
        m1.setImageUrl("https://ftmp.helios.pl/Get/file/mvpstr/13094/1542632610");
        movies.add(m1);

    }
}
