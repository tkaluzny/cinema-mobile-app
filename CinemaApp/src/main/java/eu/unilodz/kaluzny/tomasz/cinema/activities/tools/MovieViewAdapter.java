package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.activities.MovieDetailsActivity;
import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;

import static android.support.v4.content.ContextCompat.startActivity;

public class MovieViewAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private Context mContext;
    private List<Movie> moviesList;
    private String seanceDate;
    private final OnMovieClickListener listener;

    public MovieViewAdapter(Context context, List<Movie> movies, String seanceDate){
        this.mContext = context;
        this.moviesList = movies;
        this.seanceDate = seanceDate;
        this.listener = new OnMovieClickListener();
    }
    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.movie_card_view, viewGroup, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int position) {
        Movie movie = moviesList.get(position);
        movieViewHolder.bind(movie, seanceDate, this.mContext, this.listener);
    }

    public class OnMovieClickListener {
        void onItemClick(Movie movie, String date){

            Intent intent = new Intent(mContext, MovieDetailsActivity.class);
            intent.putExtra("movieId",movie.getId());
            intent.putExtra("seance-date", date);
            startActivity(mContext, intent, null);

        }
    }

    @Override
    public int getItemCount() {
       return CollectionUtils.size(moviesList);
    }
}
