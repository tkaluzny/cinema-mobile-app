package eu.unilodz.kaluzny.tomasz.cinema.providers;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.TimeOfDay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.*;

import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import eu.unilodz.kaluzny.tomasz.cinema.models.MoviesTimeline;
import eu.unilodz.kaluzny.tomasz.cinema.models.ReservationsModel;

public class MoviesReservations {

    private static MoviesReservations instance = null;
    private Set<ReservationsModel> reservations;
    public static MoviesReservations getInstance(){
        if(Objects.isNull(instance)){
            instance = new MoviesReservations();
        }
        return instance;
    }

    private MoviesReservations(){
        reservations = new HashSet<>();
        Random random = new Random();
        MoviesProvider moviesProvider = MoviesProvider.getInstance();
        WeeklyMoviesTimelineProvider timelineProvider = WeeklyMoviesTimelineProvider.getInstance();
        for(Movie movie : moviesProvider.getMovies()){
            MoviesTimeline timeline = timelineProvider.getMovieTimeline(movie.getId());
            DateTime start = timeline.getStartShowing();
            DateTime end = timeline.getEndShowing();
            DateTime nextDate = start;
            do{
                for(TimeOfDay time : timeline.getHours()){
                    DateTime seanceTime = nextDate .withTime(time.getHourOfDay(), time.getMinuteOfHour(), time.getSecondOfMinute(), time.getMillisOfSecond());
                    ReservationsModel reservation = new ReservationsModel(movie.getId(), seanceTime);
                    reservation.setPlaces(new HashSet<String>());
                    int amount = random.nextInt(25) + 5;
                    List<String> places = reservationPlaces();
                    for(int k = 0; k< amount; k++){
                        int i = random.nextInt(places.size());
                        reservation.getPlaces().add(places.get(i));
                        places.remove(i);
                    }
                    reservations.add(reservation);
                }
                nextDate = nextDate.plusDays(1);
            }while(nextDate.isBefore(end));

        }

    }

    public Set<String> getReservedPlacesForMovie(long movieId, DateTime time){
        for(ReservationsModel reservation : reservations){
            if(reservation.getMovieId() == movieId &&
                    time.isEqual(time)){
                return reservation.getPlaces();
            }
        }
        return new HashSet<>();
    }

    public @NonNull List<ReservationsModel> getCurrentUserReservations(String userData){
        List<ReservationsModel> reservationsList = new LinkedList<>();
        for(ReservationsModel reservation : this.reservations){
            if(reservation.getDate().isAfterNow() && Objects.nonNull(reservation.getUserData()) &&
                    reservation.getUserData().checkUserData(userData)){
                reservationsList.add(reservation);
            }
        }
        return reservationsList;
    }

    public void addReservation(ReservationsModel reservationsModel){
        this.reservations.add(reservationsModel);
    }

    private List<String> reservationPlaces(){
        List<String> result = new ArrayList<>();
        result.addAll(Arrays.asList("b1A", "b1B", "b1C", "b1D", "b1E",
                "b2A", "b2B", "b2C", "b2D", "b2E",
                "b3A", "b3B", "b3C", "b3D", "b3E",
                "b4A", "b4B", "b4C", "b4D", "b4E",
                "b5A", "b5B", "b5C", "b5D", "b5E",
                "b6A", "b6B", "b6C", "b6D", "b6E"));
        return result;
    }
}
