package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.joda.time.DateTime;

import java.util.ArrayList;

import eu.unilodz.kaluzny.tomasz.cinema.R;

public class DatesListView extends Activity {

    private ListView list ;
    private ArrayAdapter<String> adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dates_list_view);

        list = (ListView) findViewById(R.id.datesListView);


        ArrayList<String> dates = new ArrayList<String>(7);
        for(int i = 0; i< 7; i++){
            dates.add(DateTime.now()
                    .plusDays(i)
                    .toString("dd MMMM yyyy"));
        }

        adapter = new ArrayAdapter<String>(this, R.layout.date_button, dates);
        list.setAdapter(adapter);
        list.setOnItemClickListener(onClickListener());

    }

    private AdapterView.OnItemClickListener onClickListener(){
        return new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3){
                String date = (String)adapter.getItemAtPosition(position);
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                intent.putExtra("selected-date", date);
                startActivity(intent);
            }
        };
    }
}
