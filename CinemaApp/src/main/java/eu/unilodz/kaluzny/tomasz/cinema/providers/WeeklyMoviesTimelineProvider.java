package eu.unilodz.kaluzny.tomasz.cinema.providers;

import android.annotation.TargetApi;

import org.joda.time.DateTime;
import org.joda.time.TimeOfDay;

import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import eu.unilodz.kaluzny.tomasz.cinema.models.MoviesTimeline;

@TargetApi(26)
public class WeeklyMoviesTimelineProvider {

    private Set<MoviesTimeline> timelines;
    private static WeeklyMoviesTimelineProvider instance;
    private MoviesProvider moviesProvider;


    public static WeeklyMoviesTimelineProvider getInstance(){
        if(Objects.isNull(instance)){
            instance = new WeeklyMoviesTimelineProvider();
        }
        return instance;
    }

    private WeeklyMoviesTimelineProvider(){
        timelines = new HashSet<>();
        moviesProvider = MoviesProvider.getInstance();
        for(Movie movie : moviesProvider.getMovies()){
            long movieId = movie.getId();
            MoviesTimeline moviesTimeline = new MoviesTimeline();
            moviesTimeline.setMovieId(movieId);
            moviesTimeline.setStartShowing(DateTime.now());
            DateTime endDate = DateTime.now();
            endDate.plusWeeks(1);
            moviesTimeline.setEndShowing(endDate );
            moviesTimeline.setHours(new TreeSet<TimeOfDay>());
            Random rand = new Random();
            int hoursMany = rand.nextInt(3) + 2;
            do{
                int hour = rand.nextInt(12) + 9;
                int min = rand.nextInt(4) * 15;
                TimeOfDay time = new TimeOfDay(hour, min);
                moviesTimeline.getHours().add(time);
            }while(moviesTimeline.getHours().size() < hoursMany);
            timelines.add(moviesTimeline);
        }
    }

    public MoviesTimeline getMovieTimeline(long movieId){
        for(MoviesTimeline timeline : timelines){
            if(movieId == timeline.getMovieId()){
                return timeline;
            }
        }
        return null;
    }

    public  Set<TimeOfDay> getMovieHours(long movieId){
        MoviesTimeline timeline = getMovieTimeline(movieId);
        if(Objects.isNull(timeline)){
            System.out.println("Not found timeline for movieId = " + movieId);
        }
        return timeline.getHours();
    }

}
