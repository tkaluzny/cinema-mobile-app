package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;

import java.util.List;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.activities.PlaceSelectionActivity;

import static android.support.v4.content.ContextCompat.startActivity;

public class SeanceHoursViewAdapter extends RecyclerView.Adapter<SeanceHoursViewHolder>{

    private Context mContext;
    private List<DateTime> seanceHours;
    private final OnSeanceHourClickListener listener;
    private long movieId;

    public SeanceHoursViewAdapter(long movieId, Context mContext, List<DateTime> seanceHours) {
        this.mContext = mContext;
        this.seanceHours = seanceHours;
        this.movieId = movieId;
        this.listener = new OnSeanceHourClickListener();
    }

    @NonNull
    @Override
    public SeanceHoursViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.seance_hours_card_view, viewGroup, false);
        return new SeanceHoursViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SeanceHoursViewHolder seanceHoursViewHolder, int i) {
        DateTime date = seanceHours.get(i);
        seanceHoursViewHolder.bind(this.movieId, date, this.listener);
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(this.seanceHours);
    }

    public class OnSeanceHourClickListener {
        void onItemClick(long movieId, DateTime dateTime){
            Intent intent = new Intent(mContext, PlaceSelectionActivity.class);
            intent.putExtra("movieId",movieId);
            intent.putExtra("date", dateTime.toString());
            startActivity(mContext, intent, null);

        }
    }
}
