package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.joda.time.DateTime;

import java.util.Objects;
import java.util.Set;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.activities.ui.placeselection.PlaceSelectionFragment;
import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesProvider;

import static android.support.v4.content.ContextCompat.startActivity;

public class PlaceSelectionActivity extends AppCompatActivity {

    private Button submit;
    private Button cancel;
    private DateTime dateTime;
    private long movieId;
    private PlaceSelectionFragment placeSelectionFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_selection_activity);
        submit = findViewById(R.id.submit_reserv);
        cancel = findViewById(R.id.cancel_reserv);
        String sDate = getIntent().getStringExtra("date");
        dateTime = DateTime.parse(sDate);
        movieId = getIntent().getLongExtra("movieId", -1);

        if (savedInstanceState == null) {
            placeSelectionFragment = PlaceSelectionFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_reserv, placeSelectionFragment )
                    .commitNow();
            placeSelectionFragment.setDateTime(dateTime);
            placeSelectionFragment.setMovieId(movieId);
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent, null);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Set<String> places = placeSelectionFragment.getSelectedPlace();
                if(places.size() > 0){
                    Intent intent = new Intent(getApplicationContext(), SummaryReservatonActivity.class);
                    intent.putExtra("movieId", movieId);
                    intent.putExtra("date", dateTime.toString());
                    intent.putExtra("places", places.toArray(new String[places.size()]));
                    startActivity(intent, null);
                }
            }
        });
    }
}
