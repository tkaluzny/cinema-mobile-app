package eu.unilodz.kaluzny.tomasz.cinema.models;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Movie implements Serializable {

    private long id;
    private String title;
    private String type;
    private String descr;
    private Integer duration;
    private String imageUrl;
    private List<String> actors;

}
