package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.activities.tools.GridSpacingItemDecoration;
import eu.unilodz.kaluzny.tomasz.cinema.activities.tools.MovieViewAdapter;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesProvider;
import eu.unilodz.kaluzny.tomasz.cinema.utils.DpToPxConverter;

public class MainActivity extends AbstractNavigationActivity{

    private RecyclerView recyclerView;
    private MovieViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
//        navigation.setSelectedItemId(R.id.navigation_home);
        Button changeDateButton = (Button)findViewById(R.id.changeDateButton);
        changeDateButton.setOnClickListener(onChangeDateClick());
        MoviesProvider provider = MoviesProvider.getInstance();
        Intent intent = getIntent();
        String selectedDate = intent.getStringExtra("selected-date");
        if(StringUtils.isBlank(selectedDate)){
            selectedDate = DateTime.now().toString("dd MMMM yyyy");
        }
        TextView dateText = findViewById(R.id.mainSelectedDateText);
        dateText.setText(selectedDate);
        adapter = new MovieViewAdapter(this, provider.getMovies(), selectedDate );

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, DpToPxConverter.convert(10,getResources()), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }

    private View.OnClickListener onChangeDateClick(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, DatesListView.class);
                startActivity(intent, null);
            }
        };
    }

}
