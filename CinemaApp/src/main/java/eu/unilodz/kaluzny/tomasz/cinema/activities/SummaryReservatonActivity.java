package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import eu.unilodz.kaluzny.tomasz.cinema.models.ReservationsModel;
import eu.unilodz.kaluzny.tomasz.cinema.models.UserData;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesProvider;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesReservations;

//FIXME: Dodac scroll na odwracanie telefonu
public class SummaryReservatonActivity extends AppCompatActivity {

    private TextView title;
    private TextView date;
    private TextView places;
    private TextView warning;
    private EditText username;
    private EditText phone;
    private EditText email;
    private Button submit;
    private Button cancel;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_reservaton);
        init();
        final long movieId = getIntent().getLongExtra("movieId",-1);
        final Movie movie = MoviesProvider.getInstance().getById(movieId);
        title.setText(movie.getTitle());
        final String sDate = getIntent().getStringExtra("date");
        final DateTime dateTime = DateTime.parse(sDate);
        date.setText(dateTime.toString("dd-MMM-yyyy hh:mm"));
        final String[] placesArray = getIntent().getStringArrayExtra("places");
        for(int i=0; i<placesArray.length; i++){
            if(placesArray[i].length() == 3){
                placesArray[i] = placesArray[i].substring(1, 3);
            }
        }
        places.setText(StringUtils.join(placesArray, ", "));
        Glide.with(getApplicationContext()).load(movie.getImageUrl()).into(this.image);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PlaceSelectionActivity.class);
                intent.putExtra("movieId", movieId);
                intent.putExtra("date", sDate);
                startActivity(intent, null);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                warning.setText(StringUtils.EMPTY);
                String validMessage = valid();
                if(validMessage.isEmpty()){
                    ReservationsModel reservationsModel = new ReservationsModel(movieId, dateTime);
                    fllReservationMode(reservationsModel, placesArray);
                    MoviesReservations.getInstance().addReservation(reservationsModel);
                    AlertDialog dialog = new AlertDialog.Builder(SummaryReservatonActivity.this)
                            .setTitle("Zakończenie rezerwacji")
                            .setMessage("Twoja rezerwacja została pomyślnie zapisana.\nPamiętaj aby zjawić się co najmniej pół godziny przed seansem")
                            .setNeutralButton("OK",  new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent, null);
                                }
                            })
                            .create();
                    dialog.show();

                }else{
                    warning.setText(validMessage);
                }
            }
        });

    }

    private void init(){
        title = findViewById(R.id.summary_title);
        date = findViewById(R.id.summary_date);
        places = findViewById(R.id.summary_reserv_place);
        warning = findViewById(R.id.summary_warn);
        username = findViewById(R.id.summary_username);
        phone = findViewById(R.id.summary_phone);
        email = findViewById(R.id.summary_email);
        submit = findViewById(R.id.summary_submit);
        cancel = findViewById(R.id.summary_cancel);
        image = findViewById(R.id.summary_image);
    }

    private String valid(){
        List<String> errors = new LinkedList<>();
        if(StringUtils.isBlank(username.getText())){
            errors.add("Imię i Nazwisko ");
        }
        if(StringUtils.isBlank(email.getText())){
            errors.add("E-Mail ");
        }
        if(StringUtils.isBlank(phone.getText())){
            errors.add("Numer Telefonu ");
        }
        return errors.isEmpty() ? StringUtils.EMPTY : String.format("Uzupełnij %s!", StringUtils.join(errors, ", "));

    }

    private void fllReservationMode(ReservationsModel reservationsModel, String[] placesArray){
        Set<String> placesSet = new HashSet<>();
        CollectionUtils.addAll(placesSet, placesArray);
        reservationsModel.setPlaces(placesSet);
        String sUserName = username.getText().toString();
        String sEmail = email.getText().toString();
        String sPhoneNumber = phone.getText().toString();
        UserData userData = new UserData(sUserName, sPhoneNumber, sEmail);
        reservationsModel.setUserData(userData);
    }
}
