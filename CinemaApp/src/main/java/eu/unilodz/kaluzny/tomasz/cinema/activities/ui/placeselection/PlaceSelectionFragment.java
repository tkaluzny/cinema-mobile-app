package eu.unilodz.kaluzny.tomasz.cinema.activities.ui.placeselection;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.Set;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesProvider;
import lombok.Getter;
import lombok.Setter;

//FIXME: Naprawic problem z odwracaniem telefonu, dodać scroll
public class PlaceSelectionFragment extends Fragment {

    private PlaceSelectionViewModel mViewModel;
    private TextView movieTitle;
    private Button[][] places;

    @Getter
    private Set<String> selectedPlace = new HashSet<>();
    @Setter
    private DateTime dateTime;
    @Setter
    private long movieId;

    public static PlaceSelectionFragment newInstance() {
        return new PlaceSelectionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.place_selection_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PlaceSelectionViewModel.class);
        places = fillPlacesButtons();
        movieTitle = getView().findViewById(R.id.movie_name_reserv);
        Movie movie = MoviesProvider.getInstance().getById(movieId);
        movieTitle.setText(movie.getTitle());
        mViewModel.fillReservationsMarker(places, dateTime, movieId);
        prepareOnClickAction();
    }

    private Button[][] fillPlacesButtons(){
        View view = getView();
        if(view == null){
            return new Button[0][0];
        }
       
        return new Button[][]{
                new Button[]{ view.findViewById(R.id.b1A), view.findViewById(R.id.b1B), view.findViewById(R.id.b1C), view.findViewById(R.id.b1D), view.findViewById(R.id.b1E)},
                new Button[]{ view.findViewById(R.id.b2A), view.findViewById(R.id.b2B), view.findViewById(R.id.b2C), view.findViewById(R.id.b2D), view.findViewById(R.id.b2E)},
                new Button[]{ view.findViewById(R.id.b3A), view.findViewById(R.id.b3B), view.findViewById(R.id.b3C), view.findViewById(R.id.b3D), view.findViewById(R.id.b3E)},
                new Button[]{ view.findViewById(R.id.b4A), view.findViewById(R.id.b4B), view.findViewById(R.id.b4C), view.findViewById(R.id.b4D), view.findViewById(R.id.b4E)},
                new Button[]{ view.findViewById(R.id.b5A), view.findViewById(R.id.b5B), view.findViewById(R.id.b5C), view.findViewById(R.id.b5D), view.findViewById(R.id.b5E)},
                new Button[]{ view.findViewById(R.id.b6A), view.findViewById(R.id.b6B), view.findViewById(R.id.b6C), view.findViewById(R.id.b6D), view.findViewById(R.id.b6E)}
        };
    }

    private void prepareOnClickAction(){
        for(Button[] row : places){
            for(final Button place : row){
                if(place.isClickable()){
                    place.setOnClickListener(new View.OnClickListener() {
                        @Override public void onClick(View v) {
                            String name = getResources().getResourceEntryName(place.getId());
                            if(place.getText().equals("X")){
                                place.setText("");
                                place.setBackgroundColor(Color.LTGRAY);
                                selectedPlace.remove(name);
                            }else{
                                place.setText("X");
                                selectedPlace.add(name);
                                place.setBackgroundColor(Color.GREEN);
                            }
                        }
                    });
                }
            }
        }
    }

}
