package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.models.TicketModel;
import eu.unilodz.kaluzny.tomasz.cinema.providers.TicketsProvider;

public class TicketsViewAdapter extends RecyclerView.Adapter<TicketsViewHolder>{

    private Context mContext;
    private List<TicketModel> tickets;

    public TicketsViewAdapter(Context mContext) {
        this.mContext = mContext;

        tickets = TicketsProvider.getInstance()
                .getTickets()
                    .stream()
                    .collect(Collectors.<TicketModel>toList());
    }

    @NonNull
    @Override
    public TicketsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ticket_details_tab, viewGroup, false);
        return new TicketsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketsViewHolder ticketsViewHolder, int i) {
        TicketModel model = tickets.get(i);
        ticketsViewHolder.bind(model, mContext);
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(this.tickets);
    }


}
