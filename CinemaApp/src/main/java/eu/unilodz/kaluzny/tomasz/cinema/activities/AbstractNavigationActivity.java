package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import eu.unilodz.kaluzny.tomasz.cinema.R;

public abstract class AbstractNavigationActivity extends AppCompatActivity {


    @Override
    public boolean  onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_menu, menu);
        return true;
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState){
//        super.onCreate(savedInstanceState);
//        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        navigation.setSelectedItemId(R.id.navigation_home);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.title_activity_about:
                intent = new Intent(getBaseContext(), AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.title_activity_contact:
                intent = new Intent(getBaseContext(), ContactActivity.class);
                startActivity(intent);
                return true;
            case R.id.title_activity_reservation:
                intent = new Intent(getBaseContext(), CurrentReservationsActivity.class);
                startActivity(intent);
                return true;
            case R.id.navigation_home:
                intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.navigation_prices:
                intent = new Intent(getBaseContext(), TicketsViewActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
