package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.providers.ContactsProvider;

public class ContactActivity extends AbstractNavigationActivity {

    private TextView openningHoursText;
    private TextView addressText;
    private TextView phoneNumberText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_activity);
        init();
    }

    private void init(){
        ContactsProvider provider = ContactsProvider.getInstance();
        this.openningHoursText = findViewById(R.id.openingHoursText);
        this.openningHoursText.setText(provider.getOpenningHours());
        this.addressText = findViewById(R.id.addressText);
        this.addressText.setText(provider.getAddress());
        this.phoneNumberText = findViewById(R.id.phoneNumberText);
        this.phoneNumberText.setText(provider.getPhoneNumber());
    }
}
