package eu.unilodz.kaluzny.tomasz.cinema.models;

import org.joda.time.DateTime;
import org.joda.time.TimeOfDay;

import java.util.Set;

import lombok.Data;

@Data
public class MoviesTimeline {

    private long movieId;
    private DateTime startShowing;
    private DateTime endShowing;
    private Set<TimeOfDay> hours;

}
