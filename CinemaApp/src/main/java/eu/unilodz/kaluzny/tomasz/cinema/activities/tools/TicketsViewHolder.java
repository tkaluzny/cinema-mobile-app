package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.models.TicketModel;

public class TicketsViewHolder extends RecyclerView.ViewHolder {

    private TableLayout tableLayout;
    public TicketsViewHolder(View view){
        super(view);
        this.tableLayout = view.findViewById(R.id.ticketsTable);
    }

    public void bind(TicketModel ticketModel, Context context){
        TableRow ticketNameRow = (TableRow)tableLayout.getChildAt(0);
        TextView ticketNameText = (TextView)ticketNameRow.getChildAt(0);
        ticketNameText.setText(ticketModel.getType().getName());
        for(Pair<String, Integer> pair : ticketModel.getPricesPairs()){
            TableRow row = new TableRow(context);
            row.setGravity(Gravity.CENTER);
            row.setWeightSum(1);
            row.setBackgroundColor(0);
            TextView dayName = new TextView(row.getContext());
            dayName.setTextSize(20);
            TextView price = new TextView(row.getContext());
            price.setTextSize(20);
            dayName.setText(pair.first);
            dayName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            price.setText(pair.second.toString());
            price.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            row.addView(dayName);
            row.addView(price);
            tableLayout.addView(row);
        }

    }
}
