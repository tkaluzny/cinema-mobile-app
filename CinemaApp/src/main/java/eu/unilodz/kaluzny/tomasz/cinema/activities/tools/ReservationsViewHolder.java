package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import eu.unilodz.kaluzny.tomasz.cinema.models.ReservationsModel;
import eu.unilodz.kaluzny.tomasz.cinema.models.UserData;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesProvider;

public class ReservationsViewHolder extends RecyclerView.ViewHolder{

    private TextView resCardTitle;
    private TextView resCardDate;
    private TextView resCardPlaces;
    private TextView resCardUserName;
    private TextView resCardEmail;
    private TextView resCardPhone;

    public ReservationsViewHolder(@NonNull View itemView) {
        super(itemView);
        this.resCardTitle = itemView.findViewById(R.id.resCardTitle);
        this.resCardDate = itemView.findViewById(R.id.resCardDate);
        this.resCardPlaces = itemView.findViewById(R.id.resCardPlaces);
        this.resCardUserName = itemView.findViewById(R.id.resCardUserName);
        this.resCardEmail = itemView.findViewById(R.id.resCardEmail);
        this.resCardPhone = itemView.findViewById(R.id.resCardPhone);

    }

    public void bind(ReservationsModel reservation){
        long movieId = reservation.getMovieId();
        Movie movie = MoviesProvider.getInstance().getById(movieId);
        this.resCardTitle.setText(movie.getTitle());
        this.resCardDate.setText(reservation.getDate().toString("dd-MMM-yyyy hh:mm"));
        this.resCardPlaces.setText(StringUtils.join(reservation.getPlaces(), ", "));
        UserData userData = reservation.getUserData();
        this.resCardUserName.setText(userData.getName());
        this.resCardEmail.setText(userData.getEmail());
        this.resCardPhone.setText(userData.getPhone());

    }
}
