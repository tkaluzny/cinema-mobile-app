package eu.unilodz.kaluzny.tomasz.cinema.models;

import org.joda.time.DateTime;

import java.util.Set;

import lombok.Data;

@Data
public class ReservationsModel {

    private long movieId;
    private DateTime date;
    private Set<String> places;
    private UserData userData;

    public ReservationsModel(long movieId, DateTime date) {
        this.movieId = movieId;
        this.date = date;
    }
}
