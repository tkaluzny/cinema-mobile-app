package eu.unilodz.kaluzny.tomasz.cinema.providers;

import android.util.Pair;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import eu.unilodz.kaluzny.tomasz.cinema.models.TicketModel;
import lombok.Getter;

public class TicketsProvider {

    private static TicketsProvider instance = null;

    @Getter
    private Set<TicketModel> tickets;

    public static TicketsProvider getInstance(){
        if(Objects.isNull(instance)){
            instance = new TicketsProvider();
        }
        return instance;
    }

    private TicketsProvider(){
        tickets = new HashSet<>();
        TicketModel normal = new TicketModel();
        normal.setType(TicketModel.TicketType.NORMAL);
        normal.setPricesPairs(new HashSet<Pair<String, Integer>>());
        normal.getPricesPairs().add(new Pair<String, Integer>("Pn, Wt, Śr, Pt", 25));
        normal.getPricesPairs().add(new Pair<String, Integer>("Super Czwartek", 15));
        normal.getPricesPairs().add(new Pair<String, Integer>("Sob, Niedz", 28));
        tickets.add(normal);
        TicketModel family = new TicketModel();
        family.setType(TicketModel.TicketType.FAMILY);
        family.setPricesPairs(new HashSet<Pair<String, Integer>>());
        family.getPricesPairs().add(new Pair<String, Integer>("Pn, Wt, Śr, Pt", 21));
        family.getPricesPairs().add(new Pair<String, Integer>("Super Czwartek", 15));
        family.getPricesPairs().add(new Pair<String, Integer>("Sob, Niedz", 25));
        tickets.add(family);
        TicketModel junsen = new TicketModel();
        junsen.setType(TicketModel.TicketType.JUN_SEN);
        junsen.setPricesPairs(new HashSet<Pair<String, Integer>>());
        junsen.getPricesPairs().add(new Pair<String, Integer>("Pn, Wt, Śr, Pt", 21));
        junsen.getPricesPairs().add(new Pair<String, Integer>("Super Czwartek", 15));
        junsen.getPricesPairs().add(new Pair<String, Integer>("Sob, Niedz", 25));
        tickets.add(junsen);
        TicketModel discounted = new TicketModel();
        discounted.setType(TicketModel.TicketType.STUDENT);
        discounted.setPricesPairs(new HashSet<Pair<String, Integer>>());
        discounted.getPricesPairs().add(new Pair<String, Integer>("Pn, Wt, Śr, Pt", 21));
        discounted.getPricesPairs().add(new Pair<String, Integer>("Super Czwartek", 15));
        discounted.getPricesPairs().add(new Pair<String, Integer>("Sob, Niedz", 25));
        tickets.add(discounted);
    }
}
