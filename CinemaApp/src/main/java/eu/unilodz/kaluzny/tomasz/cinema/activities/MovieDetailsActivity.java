package eu.unilodz.kaluzny.tomasz.cinema.activities;

import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.TimeOfDay;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import static eu.unilodz.kaluzny.tomasz.cinema.utils.DpToPxConverter.convert;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.activities.tools.GridSpacingItemDecoration;
import eu.unilodz.kaluzny.tomasz.cinema.activities.tools.SeanceHoursViewAdapter;
import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;
import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesProvider;
import eu.unilodz.kaluzny.tomasz.cinema.providers.WeeklyMoviesTimelineProvider;

public class MovieDetailsActivity extends AbstractNavigationActivity {
    //FIXME: Nie dziala dolny panel - repertuar, cena biletow
    private ImageView imageView;
    private TextView titleText;
    private TextView durationText;
    private TextView typeText;
    private TextView actorsText;
    private TextView descriptionText;
    private RecyclerView recyclerView;
    private SeanceHoursViewAdapter adapter;

    public MovieDetailsActivity(){
        super();

    }
    private void initActivity(){
        this.imageView = (ImageView)findViewById(R.id.summary_image);
        this.titleText = (TextView)findViewById(R.id.summary_title);
        this.durationText = (TextView)findViewById(R.id.summary_reserv_place);
        this.descriptionText = (TextView)findViewById(R.id.descriptionText);
        this.typeText = (TextView)findViewById(R.id.summary_date);
        this.actorsText = (TextView)findViewById(R.id.actorsText);
        this.recyclerView = (RecyclerView) findViewById(R.id.seance_hours_recycler_view);
        this.actorsText.setMovementMethod(new ScrollingMovementMethod());
    }
    private void fillMovieDetails(Movie movie){
        if(Objects.isNull(movie)){
            System.err.println("Null movie passed! Abort initialization activity");
            return;
        }
        Context mContext = getBaseContext();
        this.titleText.setText(movie.getTitle());
        this.durationText.setText(movie.getDuration().toString());
        this.typeText.setText(movie.getType());
        this.descriptionText.setText(movie.getDescr());
        this.actorsText.setText(StringUtils.join(movie.getActors(), ", "));
        Glide.with(mContext).load(movie.getImageUrl()).into(this.imageView);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_movie_details);
        super.onCreate(savedInstanceState);
        initActivity();
        Intent intent = getIntent();
        long movieId = intent.getLongExtra("movieId", -1L);
        String seanceDate = intent.getStringExtra("seance-date");
        if(movieId == -1){
            System.err.print("Cannot retrive movieId");
            return;
        }
        if(StringUtils.isBlank(seanceDate)){
            System.err.println("Cannot retrive seance date");
            return;
        }
        Movie movie = MoviesProvider.getInstance().getById(movieId);
        fillMovieDetails(movie);

        WeeklyMoviesTimelineProvider provider = WeeklyMoviesTimelineProvider.getInstance();
        Set<TimeOfDay> seanceTimes = provider.getMovieHours(movieId);
        List<DateTime> seanceDates = seanceTimes.stream().map(timeToDateMapper(seanceDate)).collect(Collectors.<DateTime>toList());
        adapter = new SeanceHoursViewAdapter(movieId, getBaseContext(), seanceDates);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, convert(5,getResources()), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    Function<TimeOfDay, DateTime> timeToDateMapper(final String seanceDate){

        return new Function<TimeOfDay, DateTime >() {
            @Override
            public DateTime apply(TimeOfDay time) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
                DateTime date = null;
                try {
                    Date parsed = sdf.parse(seanceDate);
                    date = new DateTime(parsed.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if(Objects.isNull(date)){
                    System.err.println("Failure attemption to parse string date");
                    return null;
                }
                return date.withTime(time.getHourOfDay(), time.getMinuteOfHour(), 0, 0);
            }
        };
    }
}
