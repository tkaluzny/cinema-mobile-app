package eu.unilodz.kaluzny.tomasz.cinema.activities.ui.placeselection;

import android.arch.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.Button;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import eu.unilodz.kaluzny.tomasz.cinema.providers.MoviesReservations;

public class PlaceSelectionViewModel extends ViewModel {

    public void fillReservationsMarker(Button[][] places, DateTime date, long movieId){
        MoviesReservations moviesReservations = MoviesReservations.getInstance();
        Set<String> reservedPlaces = moviesReservations.getReservedPlacesForMovie(movieId, date);
        List<String> colums = Arrays.asList("A", "B", "C", "D", "E");
        for(String placeId : reservedPlaces){
            int row = Integer.valueOf(String.valueOf(placeId.charAt(1))) - 1;
            int column = Integer.valueOf(colums.indexOf(String.valueOf(placeId.charAt(2))));
            Button place = places[row][column];
            place.setBackgroundColor(Color.BLUE);
            place.setClickable(false);
        }
    }
}
