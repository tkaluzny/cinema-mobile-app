package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import org.joda.time.DateTime;

import eu.unilodz.kaluzny.tomasz.cinema.R;

public class SeanceHoursViewHolder extends RecyclerView.ViewHolder {

    private Button hourButton;
    public  SeanceHoursViewHolder(View view){
        super(view);
        this.hourButton = view.findViewById(R.id.seanceDateButto);
    }
    public void bind(final long movieId, final DateTime seanceDate, final SeanceHoursViewAdapter.OnSeanceHourClickListener listener){
        String hour = seanceDate.toString("HH:mm");
        this.hourButton.setText(hour);
        hourButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(movieId, seanceDate);
            }
        });

    }

}
