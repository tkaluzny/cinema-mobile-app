package eu.unilodz.kaluzny.tomasz.cinema.activities.tools;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.TimeOfDay;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import eu.unilodz.kaluzny.tomasz.cinema.R;
import eu.unilodz.kaluzny.tomasz.cinema.models.Movie;

import static eu.unilodz.kaluzny.tomasz.cinema.providers.WeeklyMoviesTimelineProvider.getInstance;

public class MovieViewHolder extends RecyclerView.ViewHolder{

    TextView title;
    TextView hours;
    ImageView thumbnail;
    public MovieViewHolder(View view) {
        super(view);
        this.title = (TextView) view.findViewById(R.id.title);
        this.thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        this.hours = (TextView) view.findViewById(R.id.hoursListText);
    }

    public void bind(final Movie movie, final String date, Context mContext, final MovieViewAdapter.OnMovieClickListener listener){
        this.title.setText(movie.getTitle());
        Set<TimeOfDay> timeSet = getInstance().getMovieHours(movie.getId());
        String hoursList = StringUtils.join(timeSet.stream().map(convertTime()).collect(Collectors.<String>toList()), ", ");
        hours.setText(hoursList);
        Glide.with(mContext).load(movie.getImageUrl()).into(thumbnail);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(movie, date);
            }
        });
        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(movie, date);
            }
        });
    }

    private Function<TimeOfDay, String> convertTime(){
        return new Function<TimeOfDay, String>() {
            @Override
            public String apply(TimeOfDay input) {
                return input.toString("HH:mm");
            }
        };
    }
}